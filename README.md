**Description**


This **Word Press** plugin makes it easy for you to add your Google Analytics tracking code as well as your meta tag verification of Webmaster Tools. NEW - Now you are able to add your Google AdWords conversion tracking code and select a list of pages from a dropdown to choose which page you want the code to be placed on (e.g. a thank you page).

 **Installation**


1. Upload the entire folder to the `/wp-content/plugins/` directory.


2. Activate the plugin through the 'Plugins' menu in WordPress.


3. Add your Google Analytics tracking code, Webmaster Tools Verification HTML Snippet and/or your Google AdWords Conversion Tracking code to the Google Code settings.